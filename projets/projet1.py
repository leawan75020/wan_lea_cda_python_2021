import random
ENEMY_HEALTH = 50
PLAYER_HEALTH = 50
NUMBER_OF_POTION = 3
SKIP_TURN = False


while True:
    # Jeu du joueur
    if SKIP_TURN:
        print("Vous passez votre tour...")
        SKIP_TURN = False
    else:
        user_choice = ""

        while user_choice not in ["1", "2"]:
            user_choice = input("entre votre choix ,(1) Attack; (2) Potion, 1 ou 2, SVP   ")
         
            if user_choice == "1":
                your_attack = random.randint(5,10)
                ENEMY_HEALTH -=your_attack
                print(f"vous avez infligé  {your_attack}  points de dégats a l'ennemy")
                print(f"ENEMY_HEALTH est {ENEMY_HEALTH} PLAYER_HEALTH est {PLAYER_HEALTH}")
            
            elif user_choice== "2":
                if NUMBER_OF_POTION > 0:
                    potion_health = random.randint(15,50)
                    PLAYER_HEALTH += potion_health
                    NUMBER_OF_POTION -= 1
                    print(f"Vous récupérez {potion_health} points de vie")
                    print(f"ENEMY_HEALTH est {ENEMY_HEALTH} PLAYER_HEALTH est {PLAYER_HEALTH}")
                    SKIP_TURN = True

                else:
                    print(f"vous n'avez pas la potion.")
                    continue

    if ENEMY_HEALTH > 0:
        enemy_attack = random.randint(5,15)
        PLAYER_HEALTH -= enemy_attack
        print(f"Enemy vous attack {enemy_attack} point de dégats a vous.ENEMY_HEALTH:{ENEMY_HEALTH}, PLAYER_HEALTH:{PLAYER_HEALTH}")
        if PLAYER_HEALTH <= 0:
            print(f"Enemy a gagné, vous etes mort.")
            break
    else:
        print(f"Vous avez gagné, enemy est mort.")
        break

print("Le Jeux est fini, a bientôt.===============================================")
